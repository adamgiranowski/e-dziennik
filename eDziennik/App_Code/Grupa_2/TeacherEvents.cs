﻿using eDziennik.App_SchoolDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eDziennik.App_Code.Grupa_2
{
    public class TeacherEvents
    {
        public List<Event> GetEaventsByTeacherId(int teacherId)
        {
            List<Event> events;
            using (SchoolDb db = new SchoolDb())
            {
                events = db.Events.Where(eve => eve.TeacherId == teacherId).ToList();
            }
            return events;
        }
        public void AddEvent(Event eve)
        {
            using (SchoolDb db = new SchoolDb())
            {
                db.Events.Add(eve);
                db.SaveChanges();
            }
        }
        public void UpdateEvent()
        {
            using (SchoolDb db = new SchoolDb())
            {
                db.Events.Find();
                db.SaveChanges();
            }
        }
    }
}