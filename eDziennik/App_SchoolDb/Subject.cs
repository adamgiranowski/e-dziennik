//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from eve template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eDziennik.App_SchoolDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class Subject
    {
        public Subject()
        {
            this.Grades = new HashSet<Grade>();
            this.Teachers = new HashSet<Teacher>();
            this.Classes = new HashSet<Class>();
        }
    
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
    
        public virtual ICollection<Grade> Grades { get; set; }
        public virtual ICollection<Teacher> Teachers { get; set; }
        public virtual ICollection<Class> Classes { get; set; }
    }
}
