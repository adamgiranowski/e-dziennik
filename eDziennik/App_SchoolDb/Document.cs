//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from eve template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eDziennik.App_SchoolDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class Document
    {
        public int DocumentId { get; set; }
        public int StudentId { get; set; }
        public int TeacherId { get; set; }
        public DocumentType DocumentType { get; set; }
        public System.DateTime DocumentDate { get; set; }
        public string DocumentContent { get; set; }
    
        public virtual Student Student { get; set; }
        public virtual Teacher Teacher { get; set; }
    }
}
