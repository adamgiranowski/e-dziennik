﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SchoolMasterPage.Master" AutoEventWireup="true" CodeBehind="TeacherEventsViewEdit.aspx.cs" Inherits="eDziennik.App_View.Grupa_3.TeacherEventsViewEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" >
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >
    <div style="width: 160%; height: 31px;" align ="right">
        <asp:Button ID="AddButton" runat="server" OnClick="AddButton_Click" style="margin-left: 0px" Text="Dodaj" Width="72px" />
        <asp:Button ID="EndButton" runat="server" OnClick="Button1_Click" Text="Zakończ" />
    </div>
    <div style="width: 160%">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" DataKeyNames="EventId" EmptyDataText="There are no data records to display." ForeColor="Black" Style="margin-right: 189px" Width="100%" AllowPaging="True" AllowSorting="True" PageSize="2" CellSpacing="2" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating">
            <Columns>
                <asp:BoundField DataField="TeacherId" HeaderText="TeacherId" SortExpression="TeacherId" Visible="False" />
                <asp:BoundField DataField="EventId" HeaderText="EventId" ReadOnly="True" SortExpression="EventId" Visible="False" />
                <asp:BoundField DataField="EventContent" HeaderText="EventContent" />
                <asp:BoundField DataField="EventData" HeaderText="EventData" DataFormatString="{0:U}" />
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            </Columns>
            <FooterStyle BackColor="#CCCCCC" />
            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
            <RowStyle BackColor="White" />
            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#808080" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#383838" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ProjektGrupowyConnectionString1 %>" DeleteCommand="DELETE FROM [Events] WHERE [EventId] = @EventId" InsertCommand="INSERT INTO [Events] ([TeacherId], [EventContent], [EventData]) VALUES (@TeacherId, @EventContent, @EventData)" ProviderName="<%$ ConnectionStrings:ProjektGrupowyConnectionString1.ProviderName %>" SelectCommand="SELECT [EventId], [TeacherId], [EventContent], [EventData] FROM [Events]" UpdateCommand="UPDATE [Events] SET [TeacherId] = @TeacherId, [EventContent] = @EventContent, [EventData] = @EventData WHERE [EventId] = @EventId">
            <DeleteParameters>
                <asp:Parameter Name="EventId" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="TeacherId" Type="Int32" />
                <asp:Parameter Name="EventContent" Type="String" />
                <asp:Parameter Name="EventData" Type="DateTime" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="TeacherId" Type="Int32" />
                <asp:Parameter Name="EventContent" Type="String" />
                <asp:Parameter Name="EventData" Type="DateTime" />
                <asp:Parameter Name="EventId" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>

    </div>
</asp:Content>
