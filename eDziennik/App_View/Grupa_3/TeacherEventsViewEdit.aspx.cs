﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using eDziennik.App_SchoolDb;
using eDziennik.App_Code.Grupa_3;

namespace eDziennik.App_View.Grupa_3
{
    public partial class TeacherEventsViewEdit : System.Web.UI.Page
    {
        private TeacherEvents teacherEvents;
        protected void Page_Load(object sender, EventArgs e)
        {
            teacherEvents = new TeacherEvents();
            //usunąć dodawanie TeacherID do sesji 
            Session.Add("TeacherId", 1);
            // ^^
            if (!IsPostBack)
            {                
                this.bindData();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("TeacherEventsView.aspx");
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("TeacherEventsViewAdd.aspx");
        }



        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            this.bindData();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            this.bindData();
        }
        private void bindData()
        {
            int teacherId = (int)Session["TeacherId"];
            List<Event> events = teacherEvents.GetEaventsByTeacherId(teacherId);
            events.Sort((x, y) => x.EventData.CompareTo(y.EventData));
            GridView1.DataSource = events;
            GridView1.DataBind();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            Event eve = new Event();
            eve.EventId = (int)this.GridView1.DataKeys[e.RowIndex].Value;
            eve.EventData = DateTime.Parse(e.NewValues[1].ToString());
            eve.EventContent = e.NewValues[0].ToString();          
            
           this.teacherEvents.UpdateEvent(eve);
            GridView1.EditIndex = -1;
            this.bindData();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;

            this.bindData();

        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = e.RowIndex;
            int idEvent = (int)this.GridView1.DataKeys[index].Value;            
            this.teacherEvents.RemoveEvent(idEvent);
            this.bindData();
        }

    }
}