﻿using eDziennik.App_Code.Grupa_3;
using eDziennik.App_SchoolDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace eDziennik.App_View.Grupa_3
{
    public partial class TeacherEventsView : System.Web.UI.Page
    {
        private TeacherEvents teacherEvents;
        protected void Page_Load(object sender, EventArgs e)
        {
            teacherEvents = new TeacherEvents();
            //usunąc
            Session.Add("TeacherId", 1);
            if (!IsPostBack)
            {               
                this.bindData();
            }
        }

        protected void EditButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("TeacherEventsViewEdit.aspx");
        }

        private void bindData()
        {
            int teacherId = (int)Session["TeacherId"];
            List<Event> events = teacherEvents.GetEaventsByTeacherId(teacherId);
            events.Sort((x, y) => x.EventData.CompareTo(y.EventData));
            this.GridView1.DataSource = events;
            this.GridView1.DataBind();
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            this.bindData();
        }
    }
}