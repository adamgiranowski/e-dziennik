﻿using eDziennik.App_Code.Grupa_3;
using eDziennik.App_SchoolDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;



namespace eDziennik.App_View.Grupa_3
{
    
    public partial class TeacherEventsViewAdd : System.Web.UI.Page
    {
        private TeacherEvents teacherEvents;
        protected void Page_Load(object sender, EventArgs e)
        {
            teacherEvents = new TeacherEvents();
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            Event eve = new Event();
            eve.EventContent = this.ContentTextBox.Text;
            eve.EventData = DateTime.Parse(this.DateTextBox.Text);
            eve.TeacherId = (int)Session["TeacherId"];
            teacherEvents.AddEvent(eve);
            Response.Redirect("TeacherEventsViewEdit.aspx");
        }

        protected void AnulujButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("TeacherEventsViewEdit.aspx");
        }
    }
}