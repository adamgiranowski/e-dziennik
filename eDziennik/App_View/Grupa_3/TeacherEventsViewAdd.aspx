﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SchoolMasterPage.Master" AutoEventWireup="true" CodeBehind="TeacherEventsViewAdd.aspx.cs" Inherits="eDziennik.App_View.Grupa_3.TeacherEventsViewAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:Label ID="ContentLabel" runat="server" Text="Treść"></asp:Label>
    </div>
    <div>
        <asp:TextBox ID="ContentTextBox" runat="server" Height="55px" TextMode="MultiLine" Width="331px"></asp:TextBox>
    </div>
    <div>
        <asp:Label ID="DateLabel" runat="server" Text="Data"></asp:Label>
    </div>
    <div>
        <asp:TextBox ID="DateTextBox" runat="server" TextMode="Date" Width="326px"></asp:TextBox>       
    </div>
    <div>
        <asp:Button ID="AnulujButton" runat="server" OnClick="AnulujButton_Click" Text="Anuluj" />
        <asp:Button ID="AddButton" runat="server" Text="Dodaj" OnClick="AddButton_Click" />
    </div>

</asp:Content>
